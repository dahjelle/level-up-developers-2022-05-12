# Level Up Developers 2022-05-12

![aspects of good software](./software-axes.svg)

## Book List

### Read and Recommend

in no particular order…

- [Agile Conversations](https://www.amazon.com/Agile-Conversations-Transform-Your-Culture-ebook/dp/B07YZP8LC9/ref=sr_1_1?crid=A1ZZ4BAPUJQG&keywords=agile+conversations&qid=1652294256&sprefix=agile+conversations%2Caps%2C130&sr=8-1) by Jeffrey Fredrick and Douglas Squirrel
- [Structure and Interpretation of Computer Programs](https://www.amazon.com/Structure-Interpretation-Computer-Programs-Engineering/dp/0262510871/ref=sr_1_1?crid=3IBDBLPL17XBA&keywords=Structure+and+Interpretation+of+Computer+Programs&qid=1652293739&sprefix=structure+and+interpretation+of+computer+programs%2Caps%2C122&sr=8-1) by Abelson and Sussman
- [The Effective Engineer](https://www.amazon.com/Effective-Engineer-Engineering-Disproportionate-Meaningful/dp/0996128107/ref=sr_1_1?crid=17LBKR0CAAQUZ&keywords=the+effective+engineer&qid=1652389603&sprefix=the+effective+enginee%2Caps%2C154&sr=8-1) by Edmond Lau
    - I did a [FullStack talk](https://gitlab.com/dahjelle/the-effective-engineer-review-2021-02-11/-/blob/main/src/The%20Effective%20Engineer%20Book%20Review%20and%20Summary.md) on it last year if you want the highlights.
- [The Humane Interface](https://www.amazon.com/Humane-Interface-Directions-Designing-Interactive/dp/0201379376/ref=sr_1_1?crid=2ILPK8C9YKPZ4&keywords=the+humane+interface&qid=1652293754&sprefix=the+humane+interface%2Caps%2C114&sr=8-1) by Jef Raskin
- [The Mythical Man Month](https://www.amazon.com/Mythical-Man-Month-Software-Engineering-Anniversary/dp/0201835959/ref=sr_1_1?crid=38J42N5VJ3UEN&keywords=The+Mythical+Man+Month&qid=1652293772&sprefix=the+mythical+man+month%2Caps%2C90&sr=8-1) by Frederick Brooks
- [The Art of Agile Development](https://www.amazon.com/Art-Agile-Development-James-Shore/dp/1492080691/ref=sr_1_1?crid=3DDXRGUBGMSDN&keywords=The+Art+of+Agile+Development&qid=1652293795&sprefix=the+art+of+agile+development%2Caps%2C90&sr=8-1) by James Shore and Shane Warden
- [The Programmer's Brain](https://www.amazon.com/Programmers-Brain-every-programmer-cognition-ebook/dp/B09C6LN8XT/ref=sr_1_1?crid=2TUWP5BGZPDHI&keywords=The+Programmer%27s+Brain&qid=1652293818&sprefix=the+programmer%27s+brain%2Caps%2C94&sr=8-1) by Felienne Hermans
- [Righting Software](https://www.amazon.com/Righting-Software-Juval-Löwy/dp/0136524036/ref=sr_1_1?crid=1X9SQL3EJ1YOH&keywords=Righting+Software&qid=1652293840&sprefix=righting+software%2Caps%2C93&sr=8-1) by Juval Löwy
- [JavaScript Allongé](https://www.amazon.com/Javascript-Allongé-Reginald-Braithwaite-ebook/dp/B00FLKRCVO/ref=sr_1_1?crid=AR5MQ7E983O&keywords=JavaScript+Allongé&qid=1652293862&sprefix=javascript+allongé%2Caps%2C79&sr=8-1) by Reginald Braithwaite
- [Crafting Interpreters](https://www.craftinginterpreters.com) by Robert Nystrom
- [Software Design Decoded](https://www.amazon.com/Software-Design-Decoded-Experts-Think/dp/0262035189/ref=sr_1_1?crid=1GLWIWLE1D38H&keywords=software+design+decoded&qid=1652293929&sprefix=software+design+decoded%2Caps%2C129&sr=8-1) by Marian Petre and Andre Van Der Hoek
- [Exercises in Programming Style](https://www.amazon.com/Exercises-Programming-Style-Cristina-Videira/dp/0367350203/ref=sr_1_1?crid=G4Y6AUSWCPPL&keywords=Exercises+in+Programming+Style&qid=1652293960&sprefix=exercises+in+programming+style%2Caps%2C94&sr=8-1) by Cristina Lopes
- [The Design of Everyday Things](https://www.amazon.com/Design-Everyday-Things-Revised-Expanded-ebook/dp/B00E257T6C/ref=sr_1_1?crid=3UQ077LQO6JMF&keywords=The+Design+of+Everyday+Things&qid=1652293984&sprefix=the+design+of+everyday+things%2Caps%2C140&sr=8-1) by Don Norman

### Heard Recommended

- [The Art of Computer Programming](https://www.amazon.com/Computer-Programming-Volumes-1-4A-Boxed/dp/0321751043/ref=sr_1_1?crid=1A5X7AXHYOPIQ&keywords=The+Art+of+Computer+Programming&qid=1652294002&sprefix=the+art+of+computer+programming%2Caps%2C103&sr=8-1) by Donald Knuth
- [Software Craftsmanship](https://www.amazon.com/Software-Craftsmanship-Imperative-Pete-McBreen/dp/0201733862/ref=sr_1_3?crid=8CXBL3XZ4T2D&keywords=Software+Craftsmanship&qid=1652294016&sprefix=software+craftsmanship%2Caps%2C107&sr=8-3) by Pete Mcbreen
- [Eloquent Javascript](https://www.amazon.com/Eloquent-JavaScript-3rd-Introduction-Programming/dp/1593279507/ref=sr_1_1?crid=3H3K131SEBMXE&keywords=eloquent+javascript&qid=1652294051&sprefix=eloquent+javascript%2Caps%2C107&sr=8-1) by Marjin Haverbeke
- [The Pragmatic Programmer](https://www.amazon.com/Pragmatic-Programmer-Anniversary-Journey-Mastery/dp/B0833FBNHV/ref=sr_1_1?crid=13ZAAEJQ7IB21&keywords=The+Pragmatic+Programmer&qid=1652294064&sprefix=the+pragmatic+programmer%2Caps%2C107&sr=8-1) by Andy Hunt and Dave Thomas
- [Clean Code](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship-ebook/dp/B001GSTOAM/ref=sr_1_1?crid=31ZK4KUYF4JK1&keywords=Clean+Code&qid=1652294093&sprefix=clean+code%2Caps%2C120&sr=8-1) by Robert C. Martin

